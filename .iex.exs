c "example/repo.exs"
c "example/has_friend.exs"
c "example/person.exs"
c "example/graph.exs"

DurangoExample.Repo.start_link([])

require Durango.Query

alias DurangoExample.Person
alias DurangoExample.HasFriend
alias DurangoExample.Repo
alias DurangoExample.Graph