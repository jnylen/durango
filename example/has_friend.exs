defmodule DurangoExample.HasFriend do
  use Durango.Document

  document :has_friend, :edge do
    field :best_friend, :boolean, [default: false]
  end
end
