use Mix.Config

config :durango, DurangoExample.Repo,
  scheme:   "http",
  host:     "arangodb",
  port:     8529,
  username: "root",
  password: "durango_dev",
  database: "durango_dev"