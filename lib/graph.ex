defmodule Durango.Graph do
    defmacro  __using__(_) do
        quote do
            require Durango.Graph
            import Durango.Graph
            Module.register_attribute(__MODULE__, :relationships, accumulate: true)

            def as_struct(from, relationship, to, data \\ %{}) do
                # ENSURE later on
                data
                |> Map.put(:_to, to["_id"])
                |> Map.put(:_from, from["_id"])
                |> relationship.__struct__
            end
        end
    end
  
    defmacro graph(collection, block) when is_atom(collection) do
        quote do
            def __document__(:is_document?) do
                false
            end

            def __graph__(:is_graph?) do
                true
            end
            def __graph__(:collection) do
                unquote(collection)
            end

            # Relationships
            unquote(block)
            def __graph__(:relationships) do
                @relationships
            end
            #defstruct Keyword.keys(@relationships)
        end
    end

    defmacro relationship(from, relationship, to) do
        {relationship, from, to} = {Macro.expand(relationship, __CALLER__), Macro.expand(from, __CALLER__), Macro.expand(to, __CALLER__)}
        quote do
          Module.put_attribute(__MODULE__, :relationships, %{from: unquote(from), to: unquote(to), relationship: unquote(relationship)})
        end
    end
end