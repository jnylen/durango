defmodule Durango.Repo do
  # use GenServer

  defmacro __using__(using_opts) do
    quote do
      def __config__() do
        case :ets.lookup(__MODULE__, :config) do
          [{_, config}] ->
            config

          _ ->
            raise "Config not found for #{__MODULE__}"
        end
      end

      @twelve_hours 1000 * 60 * 60 * 12

      defp get_key(key, overrides_kwargs, config_kwargs, default \\ nil) do
        Keyword.get(overrides_kwargs, key) || Keyword.get(config_kwargs, key) || default
      end

      def start_link(passed_options \\ []) do
        using_options = unquote(using_opts)
        otp_app = passed_options[:otp_app] || using_options[:otp_app]

        if is_nil(otp_app) do
          message = "Durango.Repo requires key :otp_app keyword when using `use Durango.Repo`"
          raise %ArgumentError{message: message}
        end

        configured_options = Application.get_env(otp_app, __MODULE__, [])

        uri = %URI{
          scheme: get_key(:scheme, passed_options, configured_options),
          host: get_key(:host, passed_options, configured_options),
          port: get_key(:port, passed_options, configured_options)
        }

        configuration = %{
          name: __MODULE__,
          token_refresh_interval:
            get_key(:token_refresh_interval, passed_options, configured_options, @twelve_hours),
          username: get_key(:username, passed_options, configured_options),
          password: get_key(:password, passed_options, configured_options),
          uri: get_key(:uri, passed_options, configured_options, uri),
          database: get_key(:database, passed_options, configured_options)
        }

        :ets.new(__MODULE__, [:named_table, :set, :protected])
        :ets.insert(__MODULE__, {:config, configuration})
        GenServer.start_link(__MODULE__, configuration, name: __MODULE__)
      end

      def init(state) do
        refresh_token(state)
        {:ok, state}
      end

      defp refresh_token(state) do
        Durango.Repo.Auth.refresh_token(__MODULE__)
        Process.send_after(self(), :refresh_token, state.token_refresh_interval)
      end

      def handle_info(:refresh_token, state) do
        refresh_token(state)
        {:noreply, state}
      end

      alias Durango.Query

      def execute(%Query{} = q) do
        json = Query.to_json(q)
        Durango.Api.Cursors.create_cursor(__MODULE__, json)
      end

      def insert(item) do
        Durango.Repo.insert(__MODULE__, item)
      end

      def insert!(item) do
        Durango.Repo.insert!(__MODULE__, item)
      end

      def get(module, key) when is_atom(module) and is_binary(key) do
        Durango.Repo.get(__MODULE__, module, key)
      end

      def get!(module, key) when is_atom(module) and is_binary(key) do
        Durango.Repo.get!(__MODULE__, module, key)
      end

      def update(item) do
        Durango.Repo.update(__MODULE__, item)
      end

      def update!(item) do
        Durango.Repo.update!(__MODULE__, item)
      end

      def delete(item) do
        Durango.Repo.delete(__MODULE__, item)
      end

      def delete!(item) do
        Durango.Repo.delete!(__MODULE__, item)
      end

      def upsert(item) do
        Durango.Repo.upsert(__MODULE__, item)
      end

      def upsert!(item) do
        Durango.Repo.upsert!(__MODULE__, item)
      end
    end
  end

  alias Durango.Query
  require Durango

  @doc """
  Execute a query
  """
  def execute(repo, %Query{} = q) do
    json = Query.to_json(q)
    Durango.Api.Cursors.create_cursor(repo, json)
  end

  defp ensure_document!(%module{}, verb) do
    ensure_document!(module, verb)
  end

  defp ensure_document!(module, verb) do
    if not Durango.Document.is_document?(module) and Durango.Document.Changeset != module do
      raise ArgumentError,
        message: """
        Durango.Repo.#{verb}/1 only takes Durango.Document or Changeset models.
        The model module #{inspect(module)} must `use Durango.Document` or Changeset and define a document.
        """
    end
  end

  @doc """
  Insert a document
  """

  def insert(repo, doc) do
    ensure_document!(doc, :insert)
    q = convert_changeset(doc) |> insert_query

    execute(repo, q)
    |> into(convert_changeset(doc))
  end

  @doc """
  Same as insert/2 but raises on error
  """
  def insert!(repo, doc) do
    case insert(repo, doc) do
      {:ok, doc} -> doc
      {:error, error} -> raise Durango.RepoError, message: "Error on insertion: #{inspect error["errorMessage"]}"
      error -> raise Durango.RepoError, message: "Unknown error on insertion: #{inspect error}"
    end
  end

  @doc """
  Convert a changeset into a document with the updated changes added.
  """
  def convert_changeset(
        %Durango.Document.Changeset{changes: changes, document: document, errors: errors} = _doc
      ) do
    if not Enum.empty?(errors) do
      raise ArgumentError,
            "A changeset has errors in it so it won't do any action. Errors:" <>
              "#{inspect(errors)}"
    else
      Map.merge(document, changes)
    end
  end

  def convert_changeset(doc), do: doc

  @doc """
  Create the insertion query
  """
  def insert_query(%doc_collection{} = doc) do
    Durango.query(
      insert: ^doc,
      into: ^doc_collection,
      return: NEW
    )
  end

  @doc """
  Update a document. _id is required.
  """
  def update(repo, doc) do
    ensure_document!(doc, :update)
    q = convert_changeset(doc) |> update_query

    execute(repo, q)
    |> into(convert_changeset(doc))
  end

  @doc """
  Same as update/2 but raises on error
  """
  def update!(repo, doc) do
    case update(repo, doc) do
      {:ok, doc} -> doc
      {:error, error} -> raise Durango.RepoError, message: "Error on update: #{inspect error["errorMessage"]}"
      error -> raise Durango.RepoError, message: "Unknown error on update: #{inspect error}"
    end
  end

  @doc """
  Creates the update query
  """
  def update_query(%doc_collection{} = doc) do
    Durango.query(
      let: this_doc = document(^doc._id),
      update: this_doc,
      with: ^doc,
      in: ^doc_collection,
      return: NEW
    )
  end

  @doc """
  Deletes a document
  """
  def delete(repo, doc) do
    ensure_document!(doc, :delete)
    q = convert_changeset(doc) |> delete_query

    execute(repo, q)
    |> into(convert_changeset(doc))
  end

  @doc """
  Same as delete/2 but raises on error
  """
  def delete!(repo, doc) do
    case delete(repo, doc) do
      {:ok, doc} -> doc
      {:error, error} -> raise Durango.RepoError, message: "Error on delete: #{inspect error["errorMessage"]}"
      error -> raise Durango.RepoError, message: "Unknown error on delete: #{inspect error}"
    end
  end

  @doc """
  Creates the delete query
  """
  def delete_query(%doc_collection{} = doc) do
    Durango.query(
      let: item = document(^doc._id),
      remove: item,
      in: ^doc_collection,
      return: OLD
    )
  end

  @doc """
  Updates or inserts a document
  """
  def upsert(repo, doc) do
    ensure_document!(doc, :upsert)

    repo
    |> execute(upsert_query(convert_changeset(doc)))
    |> into(convert_changeset(doc))
  end

  @doc """
  Same as upsert/2 but raises on error
  """
  def upsert!(repo, doc) do
    case upsert(repo, doc) do
      {:ok, doc} -> doc
      {:error, error} -> raise Durango.RepoError, message: "Error on upsert: #{inspect error["errorMessage"]}"
      error -> raise Durango.RepoError, message: "Unknown error on upsert: #{inspect error}"
    end
  end

  @doc """
  Creates the upsert query
  """
  def upsert_query(%doc_collection{} = doc) do
    Durango.query(
      upsert: %{_id: ^doc._id},
      insert: ^doc,
      update: ^doc,
      in: ^doc_collection,
      return: NEW
    )
  end

  @doc """
  Gets an document and returns the item
  """
  def get(repo, module, key) when is_binary(key) and is_atom(module) do
    ensure_document!(module, :get)
    collection = module.__document__(:collection)
    collection_key = Path.join([to_string(collection), key])
    q = Durango.query(return: document(^collection_key))

    execute(repo, q)
    |> into(module)
    |> case do
      {:ok, [item]} -> item
      {:ok, [nil]} -> nil
      {:ok, []} -> nil
      return -> return
    end
  end

  @doc """
  Sames as get/3 but raises on error and no results
  """
  def get!(repo, module, key) when is_binary(key) and is_atom(module) do
    get(repo, module, key)
    |> case do
      nil -> raise Durango.NoResultsError, {module, key}
      {:error, error} -> raise Durango.RepoError, message: "Error on get: #{inspect error["errorMessage"]}"
      item -> item
    end
  end

  defp into(nil, _) do
    {:ok, nil}
  end

  defp into(data, module) when is_atom(module) and is_list(data) do
    data
    |> Enum.map(fn item -> into(item, module) end)
  end

  defp into(data, module) when is_atom(module) and is_map(data) do
    into(data, module.__struct__)
  end

  defp into({:ok, %{"result" => [nil]}}, _) do
    {:ok, []}
  end

  defp into({:ok, %{"code" => code, "result" => data}}, model) when code in [200, 201, 202] do
    {:ok, into(data, model)}
  end

  defp into(data, models)
       when is_list(data) and is_list(models) and length(data) == length(models) do
    Enum.zip(data, models)
    |> Enum.map(fn {datum, model} -> into(datum, model) end)
  end

  defp into([data], %_{} = model) do
    into(data, model)
  end

  defp into(data = %{}, %_{} = model) do
    data
    |> GenUtil.Map.to_atom_keys()
    |> Map.take(Map.keys(model))
    |> Enum.reduce(model, fn {key, value}, model_acc ->
      Map.put(model_acc, key, value)
    end)
  end

  defp into({:error, _} = err, _) do
    err
  end
end
