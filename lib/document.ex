defmodule Durango.Document do

  defmacro  __using__(_) do
    quote do
      require Durango.Document
      import Durango.Document
      Module.register_attribute(__MODULE__, :fields, accumulate: true)
      Module.put_attribute(__MODULE__, :fields, {:_id, nil})
      Module.put_attribute(__MODULE__, :fields, {:_key, nil})
      Module.put_attribute(__MODULE__, :fields, {:_rev, nil})

      def fetch(document, field) do
        case field do
          :_id -> {:ok, document._id}
          "_id" -> {:ok, document._id}
          value -> case is_atom(field) do
            false -> {:ok, get_in(document, [Access.key(String.to_atom(field))])}
            true -> {:ok, get_in(document, [Access.key(field)])}
          end
        end
      end
    end
  end

  defmacro document(collection, type \\ :document, block) when is_atom(collection) do
    quote do
      def __document__(:is_document?) do
        true
      end
      def __document__(:collection) do
        unquote(collection)
      end
      def __document__(:type) do
        unquote(type)
      end

      # _to and _from is for edge collections
      if unquote(type) == :edge do
        Module.put_attribute(__MODULE__, :fields, {:_to, nil})
        Module.put_attribute(__MODULE__, :fields, {:_from, nil})
      end

      # Fields
      unquote(block)
      def __document__(:fields) do
        @fields
      end

      defstruct Durango.Document.fix_fields(@fields)

      # @before_compile Durango.Document
    end
  end

  defmacro field(name, type \\ :any, options \\ []) do
    quote do
      Module.put_attribute(__MODULE__, :fields, {unquote(name),  unquote(options) |> Enum.into(%{}) |> Map.put(:type, unquote(type)) })
    end
  end

  # defmacro __before_compile__(_env) do
  #   quote do

  #   end
  # end

  def is_document?(%module{}) do
    is_document?(module)
  end
  def is_document?(module) when is_atom(module) do
    try do
      module.__document__(:is_document?)
    rescue
      _ -> false
    end
  end

  @doc """
  Adds a default type to each field if supplied.
  """
  def fix_fields(fields) do
    for {key, map} <- fields do
      case map do
        nil -> key
        %{default: default} -> {key, default}

        _ -> key
      end
    end
  end

end
