defmodule Durango.RepoError do
  @moduledoc """
  Raised at function when the returned value from ArangoDB has an error.
  """
  defexception [:message]
end

defmodule Durango.NoResultsError do
  @moduledoc """
  Raised at function when the returned value from ArangoDB has an error.
  """
  defexception [:message]

  @impl true
  def exception({module, key}) do
    msg = "did not get any results for key #{inspect key} in collection #{inspect to_string(module.__document__(:collection))}"
    %Durango.NoResultsError{message: msg}
  end
end
